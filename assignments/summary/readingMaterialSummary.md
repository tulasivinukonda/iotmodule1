# What is 'IoT'?
    The Internet of things is a system of interrelated computing devices, mechanical and digital machines provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.

## Let us have a look on the Industrial Revolution.

# The stages of Industrial Revolution:

![INDUSTRIAL REVOLUTION:](extras/industrial_revolution.png)

## Industry 3.0
Here Data is stored in databases and represented in excels
![INDUSTRY 3.0:](extras/3.0.png)

## Architecture of Industry 3.0
Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data.Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

    Sensors --> PLC's -->SCADA & ERP
![ARCHITECTURE:](extras/arch_3.0.png)
## Communication protocols of Industry 3.0
Communication protocols are used for sending data to a central server inside the factory.
These protocols are used by sensors to send data to PLC's. These are called as fieldbus.

![COMMUNICATION PROTOCOLS:](extras/communication_3.0.png)

## Industry 4.0
Industry 4.0 is nothing but Industry 3.0 connected with internet called IoT(Internet of things)
They send you data when you are connected to Internet.

## What can you do with the data gathered from devices?
![INDUSTRY 4.0:](extras/4.0.png)
## Architecture of Industry 4.0
Data goes from Controller to Cloud via the Industry 4.0 protocols.
![ARCHITECTURE:](extras/arch_4.0.png)

## Communication protocols of Industry 4.0
These protocols are used for sending data to cloud for data analysis.

![Communication:](extras/communication_4.0.png)

# Problems with Idustry 4.0 upgrades
## Problem 1: Cost
    Industry 3.0 devices are very Expensive as a factory owner I dont want to switch to Industry 4.0 devices because they are Expensive.

## Problem 2: Downtime
    Changing Hardware means a big factory downtime, I dont want to shut down my factory to upgrade devices.

## Problem 3: Reliability
    I dont want to invest in devices which are unproven and unreliable.

# Solutions for the Problems
    Get data from Industry 3.0 devices/meters/sensors without changes to the original device and then send the data to the Cloud using Industry 4.0 devices
![SOLUTION:](extras/solution.png)

## How do we get data from Industry 3.0 device?
    Convert Industry 3.0 protocols to Industry 4.0 protocols.
![CONVERSION:](extras/conversion.png)

## Challenges in conversion
1. Expensive Hardware
2. Lack of documentation
3. Properitary PLC protocols

## How to convert?
We have a library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud
![CONVERT:](extras/howtoconvert.png)

## Road map
![ROADMAP:](extras/roadmap.png)

## IoT TDSB Tools
    1. Prometheus
    2. InfluxDB

## IoT Dash Boards
    1. Grafana
    2. ThingsBoard

## IoT Platforms
    1. AWS IOT
    2. Google IoT
    3. Azure IoT
    4. Thingsboard

## Alerts
    1. Zaiper
    2. Twilio

## How it looks inside the IoT cloud ?
![IoT CLOUD:](extras/IoT.png)







